# CHANGELOG



## v0.2.1 (2024-04-06)

### Fix

* fix(deploy): add deploy pipeline ([`8b0c16e`](https://gitlab.com/EmiBelloni/my-package/-/commit/8b0c16eff6ad79d1913f6e5425ae1ab76058fc8f))


## v0.2.0 (2024-04-06)

### Feature

* feat(addition): add addition feature ([`a3f92da`](https://gitlab.com/EmiBelloni/my-package/-/commit/a3f92daa9ab5398b14f2dfc128560bf42f9aaad5))


## v0.1.0 (2024-04-06)

### Feature

* feat(sr): add semantic release cd pipeline ([`6658cc2`](https://gitlab.com/EmiBelloni/my-package/-/commit/6658cc23819f310b91e14596d6cf9fbb8037383b))

### Unknown

* add semantic release config ([`5a912fd`](https://gitlab.com/EmiBelloni/my-package/-/commit/5a912fdb1eac97280f2baed55555ff428ea126dc))

* Create poetry.lock ([`54d718e`](https://gitlab.com/EmiBelloni/my-package/-/commit/54d718e1679327833bcc56853c011db07a48f637))

* Add pyproject.toml to init poetry ([`083096e`](https://gitlab.com/EmiBelloni/my-package/-/commit/083096e7e4d9a1d3dc65a3c909dd767d95e6cedb))

* Update .gitlab-ci.yml file ([`faf039e`](https://gitlab.com/EmiBelloni/my-package/-/commit/faf039efa082fb3287d0729a839d440f076615d4))

* Add first pipeline ([`a5429bf`](https://gitlab.com/EmiBelloni/my-package/-/commit/a5429bfb136d207bad817e8857eb3ae3da48d9fb))

* Initial commit ([`3bc9fd5`](https://gitlab.com/EmiBelloni/my-package/-/commit/3bc9fd5b2a53947f84472713d66769436ebb91f3))
